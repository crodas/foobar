import React from 'react';
import ReactDOM from 'react-dom';
import { Button, Col } from 'react-bootstrap';
import RichTextEditor from 'react-rte';
import 'whatwg-fetch';

function get(article) {
    return fetch("/v4/" + article)
        .then(r => {
            return r.json()
        });
}

class MyEditor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: RichTextEditor.createValueFromString(props.value || '', 'html'),
            version: props.version,
            no_changes: true
        }
    }
    onChange(value) {
        this.setState({value, no_changes: false})
    }
    save() {
        let data = {
            data: this.state.value.toString('html'),
            previous_version: this.state.version,
        };

        this.setState({ saving: true });

        return fetch("/v4/" + this.props.articleID, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(r => {
            return r.json();
        }).then(r => {
            this.setState({ saving: false });
            if (r.err) return this.setState({ error: r.err });
            this.setState({ version: r.new_version, no_changes: false });
        });
    }
    render() {
        let error;
        let status = '';
        if (this.state.error) {
            error = <div className="alert alert-danger">{this.state.error}</div>
            setTimeout(() => { this.setState({ error: null }) }, 5000);
        }

        if (this.state.saving || this.state.no_changes) {
            status = 'disabled';
        }

        return <div>
            {error}
            <RichTextEditor
                value={this.state.value}
                onChange={e => this.onChange(e)}
            />
            <button onClick={e=> this.save()} className={"btn btn-success " + status}>Save!</button>
            <button onClick={e=> this.props.onChange()} className={"btn btn-danger"}>Close</button>
        </div>
    }
}

class Layout extends React.Component {
    constructor(args) {
        super(args);
        this.state = { body: <div className="alert alert-success">Loading...</div>, articleID: "Latest_plane_crash" };
    }
    fetchData() {
        get(this.state.articleID).then(data => {
            this.setState({ 
                article: data.data,
                version: data.version,
                outdated: false,
                body: <div dangerouslySetInnerHTML={{__html: data.data}} /> 
            });
        });
    }
    componentWillMount() {
        this.fetchData();
    }
    watchForChanges() {
        clearTimeout(this.wd);
        if (this.state.article && !this.state.outdated) {
            this.wd = setTimeout(() => {
                get(`${this.state.articleID}/${this.state.version}`)
                    .then(r => {
                        if (!this.state.article) return;
                        this.watchForChanges();
                        this.setState({ outdated: !r.up_dated });
                    })
            }, 5000);
        }
    }
    render() {
        let edit = '';
        let update = '';
        if (this.state.article) {
            this.watchForChanges();
            if (this.state.outdated) {
                update = <div className="alert alert-success">
                    There is new version in this article
                    <button onClick={e => this.fetchData() } className="btn btn-danger">Update</button>
                </div>;
            }
            edit = <div>
                <Col xs={10}>
                    {update}
                </Col>
                <Col xs={2}>
                    <button className="btn btn-success" onClick={e=> {
                        this.setState({ body: <MyEditor 
                                value = {this.state.article} version={this.state.version}
                                articleID={this.state.articleID} onChange={r => this.fetchData() }
                        /> , article: null })
                    }}>Edit me</button>
                </Col>
                <div style={{clear: 'both'}} />
            </div>
        }
        return <Col xs={12}>
            <Col xs={2}>
                <h2>Menu</h2>
            </Col>
            <Col xs={10}>
                {edit}
                <div>{this.state.body}</div>
            </Col>
        </Col>
    }
}

ReactDOM.render(<Layout />, document.getElementById('main'));

