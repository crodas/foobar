var cluster = require('cluster');
var cpuCount = 16 || require('os').cpus().length;

if (cluster.isMaster) {
    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }
    
    cluster.on('exit', function(worker, code, signal) {
        console.log('Worker ' + worker.process.pid + ' died with code: ' + code + ', and signal: ' + signal);
        setTimeout(function() {
            console.log('Starting a new worker');
            cluster.fork();
        }, 100);
    });

} else {
    console.log('Worker %d running!', cluster.worker.id);
    require('./web.js');
}
