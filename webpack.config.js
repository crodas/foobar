var path = require('path');
var webpack = require('webpack');

var config = {
     entry: './frontend/index.jsx',
     output: {
         path: __dirname + "/public_html/",
         filename: 'app.js',
     },
     module: {
         loaders: [{
             test: /.jsx?$/,
             loader: 'babel-loader',
             exclude: /node_modules/,
             query: {
                 presets: ['stage-2', 'es2015', 'react']
             }    
         }]
    }
};

module.exports = config;
