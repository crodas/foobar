const fs = require('fs');
const crypto = require('crypto');
const lock  = require('proper-lockfile');

/**
 *  I would just say what the exer
 */
class Data {
    constructor(dir) {
        // It's ok to use the sync API,
        // this object is instance once per worker "thread"
        const stat = fs.lstatSync(dir);
        if (!stat.isDirectory()) {
            throw new Error(`${dir} must be a directory`);
        }
        this.dir = dir;
        this.index = {};
        this.loading = {};
        this.cache = {};
    }

    write(filename, data, previous = '') {
        let hash = this.getVersionId(data);
        let metadata = JSON.stringify({previous, date: new Date, hash})
        let size  = String.fromCharCode(metadata.length);
        let version = this.getVersionId(size, previous, data);
        let f = this.dir + "/"+ filename;
        let v = Math.random();
        return new Promise((success, failure) => {
            lock.lock(f, (err, release) => {
                if (err) return failure(new Error("Somebody else is saving the document now"));
    
                this.fileInfo(filename, true, (err, fileHash) => {
                    if (fileHash !== previous) {
                        release();
                        return failure(new Error("You have an outdated version, please refresh and do send your changes back"));
                    }

                    // write metadata + new content
                    let w = fs.createWriteStream(this.dir + "/" + version);
                    w.write(size)
                    w.write(metadata);
                    w.write(data);
                    w.end();

                    // write the new index-file
                    w.on('close', function(err) {
                        if (err) { 
                            release();
                            return failure(err);
                        }
                        fs.writeFile(f, version, (err, data) => {
                            if (err) {
                                release();
                                return failure(new Error(err));
                            }
                            release();
                            success(version);
                        });
                    });
                });
            });
        });
    }

    fileInfo(filename, force = null, next = null) {
        if (typeof force === 'function' && next === null) {
            next = force;
            force = false;
        }

        if (!force && this.index[filename]) {
            // We have the file info cached
            return process.nextTick(() => {
                next(null, this.index[filename]);
            });
        }

        if (this.loading[filename]) {
            // We're loading already the metadata,
            // we subscribe to the result
            return this.loading[filename].push(next);
        }


        this.loading[filename] = [next];

        let file = this.dir + "/" + filename;

        fs.readFile(file, (err, data) => {
            if (!err) { 
                this.index[filename] = data.toString();
                let w = fs.watch(file, () => {
                    delete this.index[filename];
                    w.close();
                });
            }
            this.loading[filename].map((callback) => {
                callback(err, this.index[filename] || '');
            });
            delete this.loading[filename];
        });
    }

    getVersionId(...args) {
        let hash = crypto.createHash('sha256');
        args.map((d) => {
            hash.update(d, 'binary');
        })
        return hash.digest('hex').substr(0, 14);
    }

    read(file) {
        let promise = new Promise((success, error) => {
            this.fileInfo(file, (err, version) => {
                if (err) return error(err);
                if (this.cache[version]) return success(this.cache[version]);
                fs.readFile(this.dir + "/" + version, (err, buffer) => {
                    if (err) return error(err);
                    let metadata = JSON.parse(buffer.slice(1, buffer[0]+1).toString('binary'));
                    let data = buffer.slice(1 + buffer[0]).toString();
                    this.cache[version] = {version, metadata, data};
                    success({version, metadata, data});
                });
            });
        });

        return promise;
    }
}

exports.Data = Data;
