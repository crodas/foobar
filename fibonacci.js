/**
 *  Unoptimised fibonacci function.
 */
function fib1(n) {
    return n <= 2 ? 1 : fib1(n-1) + fib1(n-2);
}

/**
 *  Second version.
 *
 *  A bit better this time, it improves recursion
 *  by caching the output (the cache is internal).
 */
function fib2(n) {
    let cache = {};
    function fib(n) {
        if (!cache[n])  {
            cache[n] = n <= 2 ? 1 : fib(n-1) + fib(n-2);
        }
        return cache[n];
    }

    return fib(n);
}

/**
 *  Like version 2 *but* the cache is 
 *  not per request/call function stack. It's kept in memory
 *  as long as the worker "thread" is alive.
 */
let fib3 = (function() {
    let cache = {};

    return function fib(n) {
        if (!cache[n])  {
            cache[n] = n <= 2 ? 1 : fib(n-1) + fib(n-2);
        }
        return cache[n];
    }
})();

module.exports = {fib1, fib2, fib3};
