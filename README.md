# Install instruction.

The project has been tested with Node `v6.2.1`

1. `npm install`. 
2. `npm install -g webpack`
3. `webpack` # Will build the frontend code
4. 
    1. `node web` will run the single "thread" webserver
    2. `node server` will run the multi-threaded webserver
    
# Features

1. Very good concurrency support. Many reads, single write.
2. Writes don't block reads.
    1. Reads may get an older version at some point in time, but they will eventually get the latest version.
    2. If a write is blocking an article, reads can happen (although they users will be reading the previous version *and that's fine*). 
4. Each change creates a new file in disk (inspired in CouchDB, but quite different) (`data.js`).
    1. Makes easy to rollback to any point in time
    2. Reads can happen while an update is taking place (although they would get the older version)
    3. If the index file is lost, it's easy to build it back
    4. The index file and content are kept in memory. We look for changes in the FS. That speeds up things a bit, but could be dangerous (a node could be serving old data). I belive the kernel of a decent OS would speed things up.

# Things to improve:

1. Use a database, something like `sqlite` to keep the file indexes and concurrency. Although `data.js` will do handle very well concurrency.
2. Give access by version/hash to the changes
3. Give API to access the history of changes of a given article.
4. When a given user attemps to save and the system refuses because they have an older version, it would be nice ([and doable](https://neil.fraser.name/software/diff_match_patch/svn/trunk/demos/demo_patch.html)) to fetch the latest version and try to merge automatically)
