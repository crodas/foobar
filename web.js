const express = require('express');
const responseTime = require('response-time')
const fibonacci = require('./fibonacci');
const bodyParser = require('body-parser');
const data = require('./data').Data;
const app = express();

const number = 34;

// our "Database" 
const io = new data(__dirname + "/data");

app.use(responseTime())
app.use(bodyParser.json());
app.use(express.static(__dirname + "/public_html"));

app.get('/v1/:article', (req, res) => {
    res.send({response: fibonacci.fib1(number)});
});

app.get('/v2/:article', (req, res) => {
    res.send({response: fibonacci.fib2(number)});
});

app.get('/v3/:article', (req, res) => {
    res.send({response: fibonacci.fib3(number)});
});

app.get("/v4/:article", (req, res) => {
    io.read(req.params.article).catch(function(err) {
        res.status(404).send({ not_found: true });
    }).then((data) => {
        res.send(data);
    });
});

app.get("/v4/:article/:version", (req, res) => {
    io.read(req.params.article).catch(function(err) {
        res.status(404).send({ not_found: true });
    }).then((data) => {
        res.send({ up_dated: data.version === req.params.version });
    });
});

app.post("/v4/:article", (req, res) => {
    io.write(req.params.article, req.body.data, req.body.previous_version)
        .then(data => {
            return res.send({ success: true, new_version: data });
        }).catch(err => {
            return res.send({ success: false, err: err.toString()});
        });
});

app.listen(3000);
